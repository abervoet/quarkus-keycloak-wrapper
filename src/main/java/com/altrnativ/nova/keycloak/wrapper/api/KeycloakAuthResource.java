package com.altrnativ.nova.keycloak.wrapper.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.altrnativ.nova.keycloak.wrapper.models.LoginRequest;

import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.representations.AccessTokenResponse;

@Path("/auth")
public class KeycloakAuthResource {
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response login(LoginRequest req) {
        // create a new instance based on the configuration defined in keycloak.json
        AuthzClient authzClient = AuthzClient.create();
        AccessTokenResponse response = authzClient.obtainAccessToken(req.username, req.password);
        String token = response.getToken();
        return Response.ok(token).build();
    }
}