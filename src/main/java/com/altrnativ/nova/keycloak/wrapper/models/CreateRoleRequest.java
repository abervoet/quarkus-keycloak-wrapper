package com.altrnativ.nova.keycloak.wrapper.models;

import java.util.List;
import java.util.Map;

import javax.ws.rs.FormParam;

import io.smallrye.common.constraint.NotNull;

public class CreateRoleRequest {
    @FormParam("name")
    @NotNull
    public String name;

    @FormParam("description")
    public String description;

    @FormParam("attributes")
    public Map<String, List<String>> attributes;

    public CreateRoleRequest(String name, String description, Map<String, List<String>> attributes) {
        this.name = name;
        this.description = description;
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return name + " " + description;
    }
}
