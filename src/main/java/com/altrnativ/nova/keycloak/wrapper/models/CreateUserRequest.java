package com.altrnativ.nova.keycloak.wrapper.models;

import java.util.List;
import java.util.Map;

import javax.ws.rs.FormParam;

import io.smallrye.common.constraint.NotNull;

public class CreateUserRequest {
    @FormParam("username")
    @NotNull
    public String username;

    @FormParam("email")
    public String email;

    @FormParam("firstname")
    public String firstname;

    @FormParam("lastname")
    public String lastname;

    @FormParam("attributes")
    public Map<String, List<String>> attributes;

    public CreateUserRequest(String username, String email, String firstname, String lastname,
            Map<String, List<String>> attributes) {
        this.username = username;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return username + " " + email;
    }
}
