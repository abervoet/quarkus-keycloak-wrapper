package com.altrnativ.nova.keycloak.wrapper.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.altrnativ.nova.keycloak.wrapper.models.CreateUserRequest;
import com.altrnativ.nova.keycloak.wrapper.providers.KeycloakProvider;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;

@Path("/users")
public class KeycloakUsersResource {
    @GET
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam String id) {
        UserResource resource = new KeycloakProvider().getRealm().users().get(id);
        UserRepresentation user = resource.toRepresentation();
        return Response.ok(user).build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response listUsers() {
        List<UserRepresentation> users = new KeycloakProvider().getRealm().users().list();
        // roles and groups are empty ??
        return Response.ok(users).build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response deleteUser(@PathParam String id) {
        UserResource resource = new KeycloakProvider().getRealm().users().get(id);
        resource.remove(); // hard delete
        return Response.ok().build();
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createUser(CreateUserRequest req) {
        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername(req.username); // unique
        user.setFirstName(req.firstname);
        user.setLastName(req.lastname);
        user.setEmail(req.email); // unique
        user.setAttributes(req.attributes);

        // user.setGroups(); // on top of default groups

        // user.setRealmRoles();
        // user.setClientRoles();

        Response response = new KeycloakProvider().getRealm().users().create(user);
        String userId = CreatedResponseUtil.getCreatedId(response);
        user.setId(userId);

        return Response.ok(user).build();
    }
}

/*
 * 
 * // Define password credential
 * CredentialRepresentation passwordCred = new CredentialRepresentation();
 * passwordCred.setTemporary(false);
 * passwordCred.setType(CredentialRepresentation.PASSWORD);
 * passwordCred.setValue("test");
 * 
 * // Set password credential
 * userResource.resetPassword(passwordCred);
 * 
 * // Get realm role "tester" (requires view-realm role)
 * RoleRepresentation testerRealmRole = realmResource.roles()//
 * .get("tester").toRepresentation();
 * 
 * // Assign client level role to user
 * userResource.roles()
 * .clientLevel(app1Client.getId()).add(Arrays.asList(userClientRole));
 * 
 * // Send password reset E-Mail
 * VERIFY_EMAIL, UPDATE_PROFILE, CONFIGURE_TOTP, UPDATE_PASSWORD,
 * TERMS_AND_CONDITIONS
 * usersRessource.get(userId).executeActionsEmail(Arrays.asList(
 * "UPDATE_PASSWORD"));
 * 
 */