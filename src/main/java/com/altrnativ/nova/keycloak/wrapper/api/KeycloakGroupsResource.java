package com.altrnativ.nova.keycloak.wrapper.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.altrnativ.nova.keycloak.wrapper.models.CreateGroupRequest;
import com.altrnativ.nova.keycloak.wrapper.providers.KeycloakProvider;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

@Path("/groups")
public class KeycloakGroupsResource {
    @GET
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response listGroups(@PathParam String id) {
        List<GroupRepresentation> groups = new KeycloakProvider().getRealm().groups().groups();
        return Response.ok(groups).build();
    }

    @GET
    @Path("{id}/members")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response listGroupMembers(@PathParam String id) {
        GroupResource resource = new KeycloakProvider().getRealm().groups().group(id);
        List<UserRepresentation> members = resource.members();
        return Response.ok(members).build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response deleteGroup(@PathParam String id) {
        GroupResource resource = new KeycloakProvider().getRealm().groups().group(id);
        resource.remove(); // hard delete
        return Response.ok().build();
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createGroup(CreateGroupRequest req) {
        GroupRepresentation group = new GroupRepresentation();
        group.setName(req.name);
        group.setAttributes(req.attributes);
        // group.setPath("test/ +name?");
        // roles
        // access ?
        // sub groups ?

        Response response = new KeycloakProvider().getRealm().groups().add(group);
        String groupId = CreatedResponseUtil.getCreatedId(response);

        group.setId(groupId);

        return Response.ok(group).build();
    }
}
