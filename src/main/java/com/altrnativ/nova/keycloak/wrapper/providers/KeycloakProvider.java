package com.altrnativ.nova.keycloak.wrapper.providers;

import com.altrnativ.nova.keycloak.wrapper.config.KeycloakConfigProperties;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;

public final class KeycloakProvider {
    private KeycloakConfigProperties _config;

    public KeycloakProvider() {
        _config = new KeycloakConfigProperties();
    }

    private Keycloak getKeycloak() {
        return KeycloakBuilder.builder()
                .serverUrl(_config.serverUrl)
                .realm(_config.realm)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(_config.clientId)
                .clientSecret(_config.clientSecret)
                .username(_config.userName)
                .password(_config.password)
                .build();
    }

    public RealmResource getRealm() {
        return getKeycloak().realm(_config.realm);
    }
}
