package com.altrnativ.nova.keycloak.wrapper.models;

import javax.ws.rs.FormParam;

import io.smallrye.common.constraint.NotNull;

public class LoginRequest {

    @FormParam("username")
    @NotNull
    public String username;

    @FormParam("password")
    @NotNull
    public String password;

    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        return username + " " + password;
    }
}
