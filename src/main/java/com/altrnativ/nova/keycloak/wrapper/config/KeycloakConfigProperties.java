package com.altrnativ.nova.keycloak.wrapper.config;

import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

public class KeycloakConfigProperties {
    public String serverUrl;
    public String realm;
    public String clientId;
    public String clientSecret;
    public String userName;
    public String password;

    private Config _config;

    public KeycloakConfigProperties() {
        _config = ConfigProvider.getConfig();
        this.serverUrl = get("keycloak.serverUrl");
        this.realm = get("keycloak.realm");
        this.clientId = get("keycloak.clientId");
        this.clientSecret = get("keycloak.clientSecret");
        this.userName = get("keycloak.userName");
        this.password = get("keycloak.password");
    }

    private String get(String key) {
        return _config.getValue(key, String.class);
    }
}
