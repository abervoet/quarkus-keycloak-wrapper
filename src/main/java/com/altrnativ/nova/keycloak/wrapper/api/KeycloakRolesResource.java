package com.altrnativ.nova.keycloak.wrapper.api;

import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.altrnativ.nova.keycloak.wrapper.models.CreateRoleRequest;
import com.altrnativ.nova.keycloak.wrapper.providers.KeycloakProvider;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;

@Path("/roles")
public class KeycloakRolesResource {
    @GET
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response listRoles(@PathParam String id) {
        List<RoleRepresentation> roles = new KeycloakProvider().getRealm().roles().list();
        return Response.ok(roles).build();
    }

    @DELETE
    @Path("{name}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response deleteRole(@PathParam String name) {
        RoleResource role = new KeycloakProvider().getRealm().roles().get(name);
        role.remove(); // hard delete
        return Response.ok().build();
    }

    @GET
    @Path("{name}/members")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response getRoleUsers(@PathParam String name) {
        RoleResource role = new KeycloakProvider().getRealm().roles().get(name);
        Set<UserRepresentation> users = role.getRoleUserMembers();
        return Response.ok(users).build();
    }

    @GET
    @Path("{name}/groups")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response getRoleGroups(@PathParam String name) {
        RoleResource role = new KeycloakProvider().getRealm().roles().get(name);
        Set<GroupRepresentation> groups = role.getRoleGroupMembers();
        return Response.ok(groups).build();
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createRole(CreateRoleRequest req) {
        RoleRepresentation role = new RoleRepresentation();
        role.setName(req.name);
        role.setDescription(req.description);
        role.setAttributes(req.attributes);
        // composite

        RolesResource roles = new KeycloakProvider().getRealm().roles();
        roles.create(role); // no id returned...

        return Response.ok(role).build();
    }
}
