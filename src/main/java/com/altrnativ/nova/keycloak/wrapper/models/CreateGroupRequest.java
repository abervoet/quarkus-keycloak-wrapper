package com.altrnativ.nova.keycloak.wrapper.models;

import java.util.List;
import java.util.Map;

import javax.ws.rs.FormParam;

import io.smallrye.common.constraint.NotNull;

public class CreateGroupRequest {
    @FormParam("name")
    @NotNull
    public String name;

    @FormParam("path")
    public String path;

    @FormParam("attributes")
    public Map<String, List<String>> attributes;

    public CreateGroupRequest(String name, String path, Map<String, List<String>> attributes) {
        this.name = name;
        this.path = path;
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return name + " " + path;
    }
}
